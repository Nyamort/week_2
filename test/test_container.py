import ast
import os
import random
import string
import subprocess
import sys
import time
import unittest

########################################################################

root_dir = os.path.abspath(sys.path[0]+'/..')
data_dir = root_dir+'/data'
exo2_dir = root_dir+'/exo2_text_parser'

default_img = "exo2:1.0"
default_container = "my-exo2"

########################################################################

def get_random_string(length):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(int(length)))


def load_ref_metrics(path):
	metrics = []
	for dirpath, subdirs, files in os.walk(path):
		for f in files:
			if f.endswith(".txt"):
				metrics.append(parse_file(os.path.join(dirpath, f)))
	return metrics


def parse_file(filename):
	with open(filename, 'r') as data_file:
		text = data_file.read().lower()
	counters = {os.path.basename(filename): 0}
	counters.update({letter:text.count(letter) for letter in string.ascii_lowercase})
	counters[os.path.basename(filename)] = sum(counters.values())
	counters = {k: v for k, v in counters.items() if v != 0 or "txt" in k}

	return counters


def are_metrics_equal(benchmark, tested):
	return benchmark == tested


########################################################################

class TestExo2Docker(unittest.TestCase):

	def docker_build(self, img_name=default_img):
		build_cmd = f"docker build -t {img_name} {exo2_dir}"
		print(build_cmd)
		trace = subprocess.run(build_cmd.split(), capture_output=True, text=True)
		print(trace.stdout)
		print(trace.stderr)
		self.assertFalse(trace.stderr)

	def docker_run(self, img_name=default_img, container_name=default_container):
		run_cmd = f"docker run -d -v {data_dir}:/data --name={container_name} {img_name}"
		print(run_cmd)
		trace = subprocess.run(run_cmd.split(), capture_output=True, text=True)
		if 'is already in use by container' in trace.stderr:
			print(f"{container_name} container already exists, cleaning...")
			self.docker_clean(container_name)
			trace = subprocess.run(run_cmd.split(), capture_output=True, text=True)
		
		self.assertFalse(trace.stderr)

	def docker_logs(self, container_name=default_container, timeout_s=5):
		start_time = time.time()
		log_cmd = f'docker logs {container_name}'
		time.sleep(1)
		print(log_cmd)
		while time.time() - start_time < timeout_s:
			trace = subprocess.run(log_cmd.split(), capture_output=True, text=True)
			if trace.stdout:
				print(f'ellapsed: {time.time() - start_time}')
				self.assertFalse(trace.stderr)
				return trace.stdout
			time.sleep(0.1)
		self.assertTrue(trace.stdout)
		return trace.stdout

	def docker_clean_if_needed(self, container_name=default_container):
		cmd = f'docker ps -a -q -f name={container_name}'
		trace = subprocess.run(cmd.split(), capture_output=True, text=True)
		if trace.stdout:
			self.docker_clean(container_name)

	def docker_clean(self, container_name=default_container):
		trace = subprocess.run(f'docker rm -f {container_name}'.split(), capture_output=True, text=True)
		print(trace.stderr)
		self.assertFalse(trace.stderr)
		
	def test_1_docker_build(self):
		self.docker_clean_if_needed()
		
		self.docker_build()
		cmd = f'docker images -q -f reference={default_img}'
		trace = subprocess.run(cmd.split(), capture_output=True, text=True)		
		self.assertTrue(trace.stdout)
		
	def test_2_docker_run(self):
		self.docker_clean_if_needed()
	
		with open(data_dir + "/random.txt", 'w') as extra_file:
			extra_file.write(get_random_string(1e5))
	
		self.docker_run()
		
		cmd = f'docker ps -a -q -f name={default_container}'
		trace = subprocess.run(cmd.split(), capture_output=True, text=True)
		
		self.assertTrue(trace.stdout)

	def test_3_docker_logs(self):
		all_metrics = load_ref_metrics(data_dir)
		
		container_logs = self.docker_logs().split('\n')
		tested_metrics = [ast.literal_eval(line) for line in container_logs if line]
		tested_metrics = [{k: v for k, v in l.items() if v != 0 or "txt" in k} for l in tested_metrics]
		self.assertTrue(tested_metrics)
		
		self.assertEqual(len(all_metrics), len(tested_metrics))
		
		for ref in all_metrics:
			names = [key for key in ref if key.endswith('.txt')]
			self.assertTrue(names)
			name = names[0]
			print(f"Verifying {name}")
			equals = [are_metrics_equal(ref, test) for test in tested_metrics]
			self.assertTrue(any(equals))
			print(f"          {name} OK")
		

########################################################################


if __name__ == '__main__':
    unittest.main()

# week_2

week 2:  building your own docker image and share data between containers
- How to share docker images
- How to share content between containers or/and the host system
- How to build your own docker image
- Exercices: build a custom docker image for your application

## How to share docker images
### Creating your own docker registry (the good)
Docker Hub is nothing special, you can create your own docker registry in a few seconds, using...Docker. Why? A simple reason would be to share images on a local network, and avoid this dreaded message from Docker Hub:

`You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limits`

You're given a quota of 100 container image pull requests per six hours by Docker Hub. Keep in mind that most traffic in and out the IUT network is bound to a single IP, and there are ~36 students pulling stuff over a 6 hours period on Fridays. That's not even 3 pull requests per person.

First, pull the *registry* image used to create a containerized docker registry server. A little trick to remember: if you try to run an image you don't have, docker will quietly look for it on Docker Hub!

`docker run -d -p 5000:5000 --restart=always --name registry registry:2 -v data:/var/lib/registry`

Here your registry is already running, bound to the 5000 port and a local volume for images persistent storage! (you will discover docker volumes in the next section).

Let's assume you have a local alpine image:

```
raph@raph-VirtualBox:~/iut/week_2$ docker images
REPOSITORY              TAG                 IMAGE ID       CREATED        SIZE
alpine        3.15                c059bfaa849c   2 months ago   5.59MB
```

You can upload it to your registry in 2 commands:

```
# The image name is what determines where it will be pushed
docker tag alpine:3.15 localhost:5000/alpine:3.15
# The image URL here matches the location and port of the local docker registry
docker push localhost:5000/alpine:3.15
```

For other machines on the same subnetwork, if you try to push now to the IP of machine hosting the server, you will see this error message:

```
raph@raph-VirtualBox:~/iut/week_2$ docker pull 172.17.0.1:5000/alpine:3.15 
Error response from daemon: Get "https://172.17.0.1:5000/v2/": http: server gave HTTP response to HTTPS client
```

You have to modify the docker daemon settings in **/etc/docker/daemon.json** to allow unsecured registries:

```
{
  "insecure-registries" : ["Your_machine_IP:Your_machine_port"]
}
```

**Note**: the default docker settings location depends on how you installed it, e.g. when installed as a snap with Ubuntu it would be in /var/snap/docker/current/config/daemon.json. 

You also need to restart the docker daemon for these changes to take effect:

```
systemctl restart dockerd
```

You can see the list of insecure registries in `docker info` output and check if your changes were applied:

```
raph@raph-VirtualBox:~/iut/week_2$ docker info | grep "Insecure Registries" -A 2
 Insecure Registries:
  172.17.0.1:5000
  127.0.0.0/8
```

Now anyone on the same network can see and pull from this registry. Obviously, **never expose an unsecured registry to a public network**!
```
raph@raph-VirtualBox:~/iut/week_2$ docker pull 172.17.0.1:5000/alpine:3.15 
3.15: Pulling from alpine
Digest: sha256:e7d88de73db3d3fd9b2d63aa7f447a10fd0220b7cbf39803c803f2af9ba256b3
Status: Downloaded newer image for 172.17.0.1:5000/alpine:3.15
172.17.0.1:5000/alpine:3.15
```

You can see with the image ID that theses 3 images are the same, only the repository used to pull them is different:

```
raph@raph-VirtualBox:~/iut/week_2$ docker images
REPOSITORY               TAG                 IMAGE ID       CREATED        SIZE
172.17.0.1:5000/alpine   3.15                c059bfaa849c   2 months ago   5.59MB
alpine                   3.15                c059bfaa849c   2 months ago   5.59MB
localhost:5000/alpine    3.15                c059bfaa849c   2 months ago   5.59MB
```


### Using docker load/save (the bad)
If you are in a hurry and can't be bothered to setup a local registry, or if you have an isolated machine, you can deploy a docker image from a tar archive. 
To create the archive from an existing image: 

`docker image save alpine:3.15 -o alpine_3.15.tar`

To load the archive from an archive:

`docker image load -i alpine_3.15.tar`

This should always be a **temporary storage and never a production-ready** way of transfering images. In this format, the image layered format cannot be used to optimize storage space, and the archive name may be completely unrelated to the image name and version.


### Copying your local docker storage (the ugly)
**Never do this**, except for very specific cases: E.g if you are building a custom Linux image, and you want to [bake docker images in the local docker storage](https://blog.savoirfairelinux.com/fr-ca/2020/comment-integrer-une-image-docker-dans-le-systeme-de-fichiers-source-de-yocto/) so they would be available immediately after flashing your target system.

Assuming you really need to do this: 
First, kill the running docker daemon:

`kill "$(pidof dockerd)"`

Empty the existing docker store directory, which is **/var/lib/docker** by default (you can set it when launching the docker daemon):

`rm -rf /val/lib/docker/*`

Pull the images you want to transfer:

`docker pull myLittleImg:1.0`

Kill the docker daemon again:

`kill "$(pidof dockerd)"`

Remove temporary files:

`rm -rf /var/lib/docker/runtimes`

`rm -rf /var/lib/docker/tmp`

You can know copy the /var/lib/docker folder in the destination filesystem.

## How to share content between containers or/and the host system
![alt text](pictures/types-of-mounts-bind.png)

### bind mounts
[Binding](https://docs.docker.com/storage/bind-mounts/) is sharing an existing file of folder on the host filesystem with a given container. Docker does not manage bound locations, and some OS do not meet the requirements for bind mounts. 

`docker run --mount src=/path/on/host,dst=/path/in/container imageName`

If the file or directory does not exist, it **will be created** on the Docker host when using the *-v* option (as a dir), while *--mount* option will **generate an error** instead. 


### docker volumes
docker [volumes](https://docs.docker.com/storage/volumes/) are cross-platform storage containers. They can be managed through the Docker API, shared between containers, and their lifecycle is separate from the containers they are used with.
The docker client can be used to handle volumes:

Create a volume:

`docker volume create myVolume`

Delete a volume:

`docker volume rm myVolume`

List existing volumes:

`docker volume ls`

Run a container with a volume mounted at a given location:

`docker run -v myVolume:/mnt/myVolume imageName`

Another syntax, using *--mount* exists:

`docker run --mount src=myVolume,dst=/mnt/myVolume imageName`

You can specify a volume mounting permission:

```
# -v syntax
docker run -v myVolume:/mnt/myVolume:ro imageName
# --mount syntax
docker run --mount src=myVolume,dst=/mnt/myVolume,readonly imageName
```

If the given volume does not exist, Docker will create it. 

### tpmfs
Docker **on Linux** has an additional option: [tpmfs](https://docs.docker.com/storage/tmpfs/). They allow the container to create files outside its writable layers, and these files will be destroyed when the container stops. As their name suggest, tmpfs are temporary mounts and only persist in the host memory. 

```
# No src argument
docker run --mount type=tmpfs, dst=/path/in/container imageName
# Alternative syntax using --tmpfs
docker run --tmpfs /path/in/container imageName
```

They are useful to store sensitive files that you don't want to keep in the host or container.

Unlike volumes and bind mounts, **tpms cannot be shared between containers**.

## Build your own docker images
![alt text](pictures/sponge-bob-believes-in-you.jpg)

You will know [build](https://docs.docker.com/engine/reference/commandline/build/) your first docker image. It will be perfect.
From this project root directory, run:

```
cd my-hello-world
docker build -t my-hello-world:1.0 .
```

This command will read the Dockerfile in my-hello-world:

```
raph@raph-VirtualBox:~/iut/week_2$ docker build -t my-hello-world:1.0 .
Sending build context to Docker daemon  145.9kB
Step 1/5 : FROM ubuntu:20.04
 ---> d13c942271d6
Step 2/5 : RUN mkdir /scripts
 ---> Running in 64c9321c2a05
Removing intermediate container 64c9321c2a05
 ---> 4847b1ed57da
Step 3/5 : RUN echo "#!/bin/bash\necho 'Hello, world!'" > /scripts/hello.sh
 ---> Running in 1c021c98de44
Removing intermediate container 1c021c98de44
 ---> 986b39eeaf80
Step 4/5 : RUN chmod +x /scripts/hello.sh
 ---> Running in a06dd729cf02
Removing intermediate container a06dd729cf02
 ---> 082b2ec46d73
Step 5/5 : CMD ["/scripts/hello.sh"]
 ---> Running in 9d66d93ce47c
Removing intermediate container 9d66d93ce47c
 ---> cfd82af7c9f1
Successfully built cfd82af7c9f1
Successfully tagged my-hello-world:1.0
```

The new image is now available locally and can be run like any other Docker images:

```
raph@raph-VirtualBox:~/iut/week_2$ docker run my-hello-world:1.0 
Hello, world!
```

You can find back the history of the layers for this image with history:
```
docker history my-hello-world:1.0
IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
9bf2808564e9   20 hours ago   /bin/sh -c #(nop)  CMD ["/scripts/hello.sh"]    0B 
1ea8c8d8b8b1   20 hours ago   /bin/sh -c chmod +x /scripts/hello.sh           31B
7eec104341d6   20 hours ago   /bin/sh -c echo -e "#!/bin/sh\necho 'Hello, …   31B
a5e64e8b3533   20 hours ago   /bin/sh -c mkdir /scripts                       0B
beae173ccac6   4 weeks ago    /bin/sh -c #(nop)  CMD ["sh"]                   0B
<missing>      4 weeks ago    /bin/sh -c #(nop) ADD file:6db446a57cbd2b7f4…   1.24MB
```

Each line in the Dockerfile will generate a layer. Dockerfiles have a very small pool of commands you can use to define your image:
- **FROM** *image:version* instruction initializes a new build stage and sets the **Base Image for subsequent instructions**. As such, a valid Dockerfile must start with a FROM instruction. The image can be any valid image – it is especially easy to start by pulling an image from the Public Repositories. The **only instruction which can precede FROM is ARG**.
- **RUN** instruction is **executing a command and committing the results to a new layer**, which will be **used for the next step** in the Dockerfile. It has 2 syntaxes:
	- **RUN** *command*, aka the *shell* form: the command is run in a shell, `/bin/sh -c` on Linux and `cmd /S /C` on Windows.
	- **RUN** *\["exec", "param1", "param2"\]*: *exec* form. Used when the base image does not have a shell, or to avoid shell string unwanted transformation.
- **COPY** *\[--chown=user:group\] src... dest* instruction **copy files in the build context**, i.e. the host working directory by default, **inside the container filesystem**.
- **ENV** *key=value* instruction is **defining an env var for all later layers and containers**. You can defined several at once, space separated.
- **ARG** *key\[=default_val\]* instruction is similar to ENV, but **can be passed as a parameter to docker build**, and **will not persist in containers** after the image is built. Do not use these to share credentials or secret variables in professional environments, use the [--secret flag](https://docs.docker.com/develop/develop-images/build_enhancements/#new-docker-build-secret-information) for docker build instead. The argument will not be used if not previously declared in the Dockerfile.
  `docker --build-arg <varname>=<value> -t img:ver .`
- **ADD** instruction is similar to COPY but also supports tar files and URLs. 
- **ENTRYPOINT** and **CMD** instructions are mostly equivalent and define the **executed command when instantiating the container**. A Dockerfile needs at least one of these two commands to be defined. When defined several, only the last CMD instruction is used. **CMD** is often used to provide default parameters to **ENTRYPOINT** command.
  ![alt text](pictures/CMD_ENTRYPOINT.png)
  Sometimes, you may want to override an entrypoint. You can do it this way:
  
  `docker run --entrypoint your_cmd image:version`
  
- **VOLUME** *path/name* instruction **prepares the creation of a mounting point** with the given name and path **to an external volume**. The volume is only created at `the docker run` call. 
- **USER** *UID:GID* command **changes the user running the next layers and in the container**. By default, the initial user is *root*.
  When running a container, you can override the given user with any existing one:
  
  `docker run -u toto image:version`
  
- **WORKDIR** *path* instruction **sets the working directory** for the next layers. Use it instead of `RUN cd path`. It also sets the container working directory!

## Exercice: build a custom docker image for your application

### Set a private docker registry

Each classroom should set 2 private docker registries on different machines, to act as intermediates with Docker Hub. They can be used to pool ressources and avoid a temporary ban from Docker Hub. *Talk* to your private registry holders and cooperate with the other students to limit the number of pulled images.
When possible, use this private registry and rename images to match the ones you will be using in your Dockerfiles.

### Prepare to commit your changes

Create a git branche name *dev/username* and work from it. Before leaving, commit and push your work on it. It will be used as a part of your evaluation.

### Build your own containerized C# application

Using this [link](https://codefresh.io/docker-tutorial/c-sharp-in-docker/) as reference, build your own docker image running in a detached container. It will print and updated periodically the system time in an exposed web server, and will close properly on SIGTERM signal (here is a little [help](https://stackoverflow.com/a/60644501) about trapping events in c#).

All files relevant to this exercice are to be stored in a *exo1_cs_app/* folder in the repository root.

### Parse a given file with the language of your choice
In the *data/* folder of this repository, there is a text file. We want to create a docker image which is **processing this data when running it and printing the results to its logs**.

The container will be created with the following command from the repository root:

`docker run --name=test-text-parser -it -v ${PWD}/data:/data text-parser:1.0`

Using C#, Java, Python or the langage of your choice, you need to prepare this Docker image to be a **valid compilation environment** (if you want to use an interpreted langage) **and execution environment**. 

Create a Dockerfile to build an image with the following goals:
- It will be able to **parse a text file** and **count each letter character occurences**, while **ignoring number, special characters**, and being **case-insensitive**. 
- It will mount the data folder from this project into the **/data location** when running the container, and **will look for any text file in it**. If there is more than one file, it will **produce one line per file** with parsing results. 

The external user creating the container only cares about the parsing output, which will be using the **exact** following format:

`{<file basename>:<sum of all letters occurrences>, <char1>: <number of occurrence1>, ..., <charN>: <number of occurenceN>}`
e.g.:
`{'lorem_ipsum.txt':161 , 'a': 123, 'b':24, 'z':14}`

Other lines are allowed and will be ignored by the evaluation script as long as they do not start with `{`.

- The *exo2_text_parser/* folder **will contain all of your work**. You are allowed to put in there:
	- Your Dockerfile.
	- Your application source code and meta data (keep it light!).
	- Shell scripts to clean containers, run the `docker run` test command, build your image. Each should not be more than 2-3 lines long.

Your docker should be handled with the 3 following commands from repository root:

`docker build -t exo2:1.0 exo2_text_parser`

`docker run -d -v $(pwd)/data:/data --name=my-exo2 exo2:1.0`

`docker container rm my-exo2`

You can test your container output by running the test script test_container.py in the repository. It will look into *data/* folder and do the same thing your container is trying to do, then will compare the container logs and the expected reply. 

`python3 test/test_container.py`

This is the same script used to **evaluate what you will commit** on your branch of this repository, and will count for this semester final mark. You can commit and push to this repository until the beginning of the next class.

## Authors
Raphaël Bouterige
